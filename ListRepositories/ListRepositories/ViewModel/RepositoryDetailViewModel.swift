//
//  RepositoryDetailViewModel.swift
//  ListRepositories
//
//  Created by Lojika IOS on 21.03.2021.
//

import Foundation
import RxSwift
import RxCocoa

class RepositoryDetailViewModel: BaseViewModel {
    
    let disposeBag = DisposeBag()
    let repository: RepositoryModel
    
    init(repository: RepositoryModel) {
        self.repository = repository
        super.init()
    }
    
    func createDataList() -> [String] {
        var dataList: [String] = []
        
        dataList.append("RepositoryName: \(repository.name)")
        dataList.append("forks_count: \(repository.forks_count)")
        dataList.append("userName: \(repository.owner.login)")
        if let description = repository.description {
            dataList.append("description: \(description)")
        }
        return dataList
    }
}
