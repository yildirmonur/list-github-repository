//
//  UserDetailViewModel.swift
//  ListRepositories
//
//  Created by Onur Yıldırım on 21.03.2021.
//

import Foundation
import RxSwift
import RxCocoa

class UserDetailViewModel: BaseViewModel {
    
    let disposeBag = DisposeBag()
    let userName: String
    let data = BehaviorRelay<[UserDetailCell]>(value: [])
    var isBottomLoading = BehaviorRelay<Bool>(value: true)
    
    init(userName: String) {
        self.userName = userName
        super.init()
        
        initialRequest(userName: userName)
    }
    
    // MARK: - Private Methods
    
    private func initialRequest(userName: String) {
        if let userRequest = getUserDetails(userName: userName),
           let repositoryRequest = getRepositories(userName: userName) {
            Observable.zip(userRequest, repositoryRequest)
                .observe(on: MainScheduler.instance)
                .subscribe { [weak self] (user, repositories) in
                    var convertedData = [UserDetailCell.user(userModel: user)]
                    repositories.forEach { convertedData.append(.repository(repository: $0)) }
                    
                    self?.data.accept(convertedData)
                } onError: { error in
                    print("Error: \(error)")
                } onCompleted: { [weak self] in
                    self?.isBottomLoading.accept(false)
                }.disposed(by: disposeBag)
        }
    }
    
    private func getUserDetails(userName: String) -> Observable<UserModel>? {
        let client = APIClient.shared
        do {
            return try client.getUserDetails(with: userName)
        }
        catch {
            print("error")
            return .none
        }
    }
    
    private func getRepositories(userName: String) -> Observable<[RepositoryModel]>? {
        let client = APIClient.shared
        do {
            return try client.getRepositories(with: userName)
        }
        catch {
            print("error")
            return .none
        }
    }
}

extension UserDetailViewModel {
    
    enum UserDetailCell {
        case user(userModel: UserModel)
        case repository(repository: RepositoryModel)
    }
}
