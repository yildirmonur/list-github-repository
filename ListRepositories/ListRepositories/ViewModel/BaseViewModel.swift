//
//  BaseViewModel.swift
//  Pockemon Search
//
//  Created by Onur Yıldırım on 17.01.2021.
//

import RxFlow
import RxSwift
import RxCocoa

class BaseViewModel: Stepper {
    
    let steps = PublishRelay<Step>()
    
}
