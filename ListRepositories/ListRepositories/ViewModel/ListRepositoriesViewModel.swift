//
//  ListRepositoriesViewModel.swift
//  ListRepositories
//
//  Created by Onur Yıldırım on 20.03.2021.
//

import Foundation
import RxSwift
import RxCocoa

class ListRepositoriesViewModel: BaseViewModel {
    
    let disposeBag = DisposeBag()
    let repositories = BehaviorRelay<[RepositoryModel]>(value: [])
    var isBottomLoading = BehaviorRelay<Bool>(value: false)
    let searchText: BehaviorRelay<String> = .init(value: "")
    private var page = 1
    private var isPagingEnabled = true
    
    override init() {
        super.init()
        subscribeToUserText()
    }
    
    // MARK: - Private Methods
    
    private func subscribeToUserText() {
        searchText.asObservable().subscribe(onNext: { [weak self] text in
            self?.page = 1
            self?.isPagingEnabled = true
            text.isEmpty ? self?.repositories.accept([]) : self?.getRepositories(text: text)
        }).disposed(by: disposeBag)
    }
    
    private func getRepositories(text: String) {
        let client = APIClient.shared
        do {
            try client.getRepositorySearchResult(with: text, page: page)?
                .observe(on: MainScheduler.instance)
                .subscribe(onNext: { [unowned self] response in
                    if let items = response.items {
                        self.isPagingEnabled ?
                            repositories.accept(items) :
                            repositories.accept(repositories.value + items)
                        self.isPagingEnabled = true
                    }
                    else {
                        self.isPagingEnabled = false
                    }
                }, onError: { [unowned self] error in
                    if !self.isPagingEnabled {
                        self.isPagingEnabled.toggle()
                        self.page -= 1
                    }
                    print(error.localizedDescription)
                },
                onCompleted: { [weak self] in
                    print("Completed event.")
                    self?.isBottomLoading.accept(false)
                }).disposed(by: disposeBag)
        }
        catch {
            print("error")
        }
    }
    
    private func increasePageNumber() {
        if isPagingEnabled {
            page += 1
            isPagingEnabled.toggle()
            isBottomLoading.accept(true)
            getRepositories(text: searchText.value)
        }
    }
    
    // MARK: - Internal Methods
    
    func endOfScrollTriggered() {
        increasePageNumber()
    }
    
    func cellSelected(index: Int) {
        steps.accept(AppSteps.repositoryDetail(repository: repositories.value[index]))
    }
    
    func avatarSelected(index: Int) {
        steps.accept(AppSteps.userDetails(userName: repositories.value[index].owner.login))
    }
}
