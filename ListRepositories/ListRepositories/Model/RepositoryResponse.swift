//
//  RepositoryModel.swift
//  ListRepositories
//
//  Created by Onur Yıldırım on 20.03.2021.
//

import Foundation

struct RepositoryResponse: Codable {
    let total_count: Int
    let items: [RepositoryModel]?
}

// MARK: - Repository
struct RepositoryModel: Codable {
    let id: Int
    let name: String
    let forks_count: Int
    let description: String?
    let owner: Owner
}

struct Owner: Codable {
    let login: String
    let avatar_url: String
}
