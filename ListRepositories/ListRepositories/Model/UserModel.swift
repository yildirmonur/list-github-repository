//
//  UserResponse.swift
//  ListRepositories
//
//  Created by Onur Yıldırım on 21.03.2021.
//

import Foundation

struct UserModel: Codable {
    
    let login: String
    let name: String?
    let company: String?
    let avatar_url: String?
}
