//
//  AppSteps.swift
//  ListRepositories
//
//  Created by Onur Yıldırım on 20.03.2021.
//

import Foundation
import RxFlow

enum AppSteps: Step {
    // List Repository ViewController
    case needsRepositories
    
    // User detail ViewController
    case userDetails(userName: String)
    
    // Repository detail
    case repositoryDetail(repository: RepositoryModel)
}

