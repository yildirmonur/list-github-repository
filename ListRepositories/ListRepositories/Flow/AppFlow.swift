//
//  AppFlow.swift
//  ListRepositories
//
//  Created by Onur Yıldırım on 20.03.2021.
//

import Foundation
import Foundation
import UIKit
import RxFlow
import RxCocoa
import RxSwift

class AppFlow: Flow {
    
    var root: Presentable {
        return self.rootViewController
    }
    private let rootViewController = UINavigationController()
    
    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? AppSteps else { return .none }
        
        switch step {
        case .needsRepositories:
            return navigateToListRepositores()
        case .userDetails(let userName):
            return navigateToUserDetails(userName: userName)
        case .repositoryDetail(let repository):
            return navigateToRepositoryDetail(repository: repository)
        }
    }
    
    private func navigateToListRepositores() -> FlowContributors {
        let viewController = ListRepositoriesVC(listRepositoriesViewModel: ListRepositoriesViewModel())
        viewController.title = "Repositories"
        
        self.rootViewController.pushViewController(viewController, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: viewController,
                                                 withNextStepper: viewController.viewModel))
    }
    
    private func navigateToUserDetails(userName: String) -> FlowContributors {
        let viewController = UserDetailVC(userDetailViewModel: UserDetailViewModel(userName: userName))
        viewController.title = "User Details"
        
        self.rootViewController.pushViewController(viewController, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: viewController,
                                                 withNextStepper: viewController.viewModel))
    }
    
    private func navigateToRepositoryDetail(repository: RepositoryModel) -> FlowContributors {
        let viewController = RepositoryDetailVC(repositortDetailViewModel: RepositoryDetailViewModel(repository: repository))
        viewController.title = "Repository Detail"
        
        self.rootViewController.pushViewController(viewController, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: viewController,
                                                 withNextStepper: viewController.viewModel))
    }
}

class AppStepper: Stepper {
    let steps = PublishRelay<Step>()
    
    static let shared = AppStepper()
    
    var initialStep: Step {
        return AppSteps.needsRepositories
    }
}
