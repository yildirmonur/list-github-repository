//
//  RepositoryCell.swift
//  ListRepositories
//
//  Created by Onur Yıldırım on 20.03.2021.
//

import UIKit
import RxSwift
import SDWebImage

class UserCell: UITableViewCell {
    
    let disposeBag = DisposeBag()
    
    private lazy var lblUserName: UILabel = {
        UILabel()
    }()
    private lazy var lblCompany: UILabel = {
        UILabel()
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupUI()
    }
    
    private func initUI() {
        selectionStyle = .none
        accessoryType = .none
        
        contentView.addSubview(lblUserName)
        contentView.addSubview(lblCompany)
    }
    
    private func setupUI() {
        lblUserName.snp.makeConstraints { m in
            m.top.equalTo(contentView.snp.top).offset(10)
            m.left.equalTo(contentView.snp.left).offset(10)
            m.right.equalTo(contentView.snp.right)
        }
        lblCompany.snp.makeConstraints { m in
            m.top.equalTo(lblUserName.snp.bottom).offset(10)
            m.left.equalTo(contentView.snp.left).offset(10)
            m.right.equalTo(contentView.snp.right)
            m.bottom.equalTo(contentView.snp.bottom).offset(-50)
        }
    }
    
    func configureUI(userName: String, company: String?) {
        lblUserName.text = userName
        guard let company = company else { return }
        lblCompany.text = company
    }
}
