//
//  RepositoryCell.swift
//  ListRepositories
//
//  Created by Onur Yıldırım on 20.03.2021.
//

import UIKit
import RxSwift
import SDWebImage

class RepositoryCell: UITableViewCell {
    
    let disposeBag = DisposeBag()
    
    var avatarImageSelectedClosure:((_ index: Int) -> ())?
    private var cellIndex: Int?
    private lazy var imgAvatar: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer()
        image.addGestureRecognizer(tapGesture)
        tapGesture.rx.event.bind { [weak self] (recognizer) in
            if let index = self?.cellIndex,
               let closure = self?.avatarImageSelectedClosure {
                closure(index)
            }
        }.disposed(by: disposeBag)
        
        return image
    }()
    
    private lazy var lblUserName: UILabel = {
        UILabel()
    }()
    
    private lazy var lblRepositoryName: UILabel = {
        UILabel()
    }()
    
    private lazy var bottomStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        stackView.alignment = .leading
        
        return stackView
    }()
    
    private lazy var lblDescription: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    
    private lazy var lblForkCount: UILabel = {
        UILabel()
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupUI()
    }
    
    private func initUI() {
        selectionStyle = .none
        
        contentView.addSubview(lblUserName)
        contentView.addSubview(imgAvatar)
        contentView.addSubview(lblRepositoryName)
        contentView.addSubview(bottomStackView)
        
        bottomStackView.addArrangedSubview(lblDescription)
        bottomStackView.addArrangedSubview(lblForkCount)
    }
    
    private func setupUI() {
        imgAvatar.snp.makeConstraints { m in
            m.left.equalTo(contentView.snp.left).offset(10)
            m.top.equalTo(contentView.snp.top).offset(3)
            m.width.height.equalTo(80)
        }
        bottomStackView.snp.makeConstraints { m in
            m.left.equalTo(contentView.snp.left).offset(10)
            m.right.equalTo(contentView.snp.right)
            m.top.equalTo(imgAvatar.snp.bottom).offset(15)
            m.bottom.equalTo(contentView.snp.bottom).offset(-30)
        }
        lblUserName.snp.makeConstraints { m in
            m.left.equalTo(imgAvatar.snp.right).offset(10)
            m.top.equalTo(contentView.snp.top).offset(10)
            m.trailing.equalTo(contentView.snp.trailing)
        }
        lblRepositoryName.snp.makeConstraints { m in
            m.left.equalTo(imgAvatar.snp.right).offset(10)
            m.top.equalTo(lblUserName.snp.bottom).offset(10)
            m.right.equalTo(contentView.snp.right)
        }
    }
    
    func configureUI(repository: RepositoryModel, index: Int?) {
        lblUserName.text = repository.owner.login
        imgAvatar.sd_setImage(with: URL(string: repository.owner.avatar_url), completed: .none)
        lblRepositoryName.text = repository.name
        cellIndex = index
    }
    
    func configureDetailUI(repository: RepositoryModel) {
        configureUI(repository: repository, index: .none)
        lblDescription.text = repository.description
        lblForkCount.text = "\(repository.forks_count)"
    }
}
