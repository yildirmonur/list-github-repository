//
//  URLService.swift
//  Pockemon Search
//
//  Created by Onur Yıldırım on 17.01.2021.
//

import Foundation

class URLService {
    
    class func getBaseUrlComponents(path: String) -> URLComponents {
        var urlComponent = URLComponents()
        
        if let scheme = Bundle.main.object(forInfoDictionaryKey: "Scheme") as? String,
           let host = Bundle.main.object(forInfoDictionaryKey: "Host") as? String {
            
            urlComponent.scheme = scheme
            urlComponent.host = host
            
            urlComponent.path = path
            return urlComponent
        }
        return urlComponent
    }
    
}
