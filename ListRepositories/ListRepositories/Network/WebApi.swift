//
//  WebApi.swift
//  ListRepositories
//
//  Created by Onur Yıldırım on 20.03.2021.
//

import Foundation
import RxCocoa
import RxSwift
// MARK: extension for converting out RecipeModel to jsonObject
fileprivate extension Encodable {
    var dictionaryValue:[String: Any?]? {
        guard let data = try? JSONEncoder().encode(self),
              let dictionary = try? JSONSerialization.jsonObject(with: data,
                                                                 options: .allowFragments) as? [String: Any] else {
            return .none
        }
        return dictionary
    }
}

class APIClient {
    static var shared = APIClient()
    lazy var requestObservable = RequestObservable(config: .default)
    
    // MARK: - Repository search results
    
    func getRepositorySearchResult(with search: String, page: Int) throws -> Observable<RepositoryResponse>? {
        
        //Comes with (scheme, host, version) order
        var urlComponent = URLService.getBaseUrlComponents(path: "/search/repositories")
        
        var queryItems = [URLQueryItem]()
        queryItems.append(URLQueryItem(name: "q", value: search))
        queryItems.append(URLQueryItem(name: "per_page", value: "10"))
        queryItems.append(URLQueryItem(name: "page", value: "\(page)"))
        urlComponent.queryItems = queryItems
        
        guard let url = urlComponent.url else {
            return .none
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField:
                            "Content-Type")
        return requestObservable.callAPI(request: request)
    }
    
    func getUserDetails(with userName: String) throws -> Observable<UserModel>? {
        
        //Comes with (scheme, host, version) order
        let urlComponent = URLService.getBaseUrlComponents(path: "/users/\(userName)")
        
        guard let url = urlComponent.url else {
            return .none
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField:
                            "Content-Type")
        return requestObservable.callAPI(request: request)
    }
    
    func getRepositories(with userName: String) throws -> Observable<[RepositoryModel]>? {
        
        //Comes with (scheme, host, version) order
        let urlComponent = URLService.getBaseUrlComponents(path: "/users/\(userName)/repos")
        
        guard let url = urlComponent.url else {
            return .none
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField:
                            "Content-Type")
        return requestObservable.callAPI(request: request)
    }
}
