//
//  BaseVC.swift
//  Pockemon Search
//
//  Created by Onur Yıldırım on 17.01.2021.
//

import UIKit

class BaseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: .none, bundle: .none)
        
        initUI()
    }
    
    func createActivityIndicator() -> UIActivityIndicatorView{
        let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.view.bounds.width, height: CGFloat(44))
        
        return spinner
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initUI() {
        view.backgroundColor = .white
    }
}
