//
//  RepositoryDetailVC.swift
//  ListRepositories
//
//  Created by Lojika IOS on 21.03.2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class RepositoryDetailVC: BaseVC {

    let disposeBag = DisposeBag()
    let viewModel: RepositoryDetailViewModel
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        stackView.alignment = .leading
        
        return stackView
    }()
    
    init(repositortDetailViewModel: RepositoryDetailViewModel) {
        self.viewModel = repositortDetailViewModel
        
        super.init(nibName: .none, bundle: .none)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        setupUI()
        
        addLabelsToStackView(dataList: viewModel.createDataList())
    }
    
    private func initUI() {
        view.addSubview(stackView)
    }
    
    private func setupUI() {
        stackView.snp.makeConstraints { m in
            m.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            m.left.equalTo(view.safeAreaLayoutGuide.snp.left)
            m.right.equalTo(view.safeAreaLayoutGuide.snp.right)
        }
    }
    
    // MARK: - Internal methods
    
    func addLabelsToStackView(dataList: [String]) {
        for data in dataList {
            let label = UILabel()
            label.numberOfLines = 0
            label.text = data
            stackView.addArrangedSubview(label)
        }
    }
}
