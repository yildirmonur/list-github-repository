//
//  UserDetailVC.swift
//  UserDetailVC
//
//  Created by Onur Yıldırım on 21.03.2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class UserDetailVC: BaseVC {
    
    let disposeBag = DisposeBag()
    let viewModel: UserDetailViewModel
    
    private var tableView:UITableView! {
        didSet {
            if let tableView = self.tableView {
                tableView.estimatedRowHeight = 100
                tableView.rowHeight = UITableView.automaticDimension
                tableView.separatorStyle = .none
                
                tableView.tableFooterView = createActivityIndicator()
                tableView.tableFooterView?.isHidden = false
                
                viewModel.isBottomLoading
                    .subscribe(onNext: { tableView.tableFooterView?.isHidden = !$0 })
                    .disposed(by: disposeBag)
                
                tableView.register(RepositoryCell.self,
                                   forCellReuseIdentifier: String(describing: RepositoryCell.self))
                tableView.register(UserCell.self,
                                   forCellReuseIdentifier: String(describing: UserCell.self))
                
                view.addSubview(tableView)
            }
        }
    }
    
    init(userDetailViewModel: UserDetailViewModel) {
        self.viewModel = userDetailViewModel
        super.init(nibName: .none, bundle: .none)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        setupUI()
        bindUI()
    }
    
    private func initUI() {
        tableView = UITableView()
    }
    
    private func setupUI() {
        tableView.snp.makeConstraints { m in
            m.edges.equalTo(view.safeAreaLayoutGuide)
        }
    }
    
    private func bindUI() {
        viewModel.data
            .bind(to: tableView.rx.items) { UserDetailVC.configureCell(tableView: $0, index: $1, item: $2) }.disposed(by: disposeBag)
    }
}

private extension UserDetailVC {
    
    // MARK: - Configure Cell
    static func configureCell(tableView: UITableView, index: Int, item: UserDetailViewModel.UserDetailCell) -> UITableViewCell {
        switch item {
        case .user(let userModel):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UserCell.self)) as? UserCell
                else { return UITableViewCell() }
            cell.configureUI(userName: userModel.login, company: userModel.company)
            cell.layoutIfNeeded()
            return cell
        case .repository(let repository):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RepositoryCell.self)) as? RepositoryCell
                else { return UITableViewCell() }
            
            cell.configureUI(repository: repository, index: index)
            cell.layoutIfNeeded()
            return cell
        }
    }
}
