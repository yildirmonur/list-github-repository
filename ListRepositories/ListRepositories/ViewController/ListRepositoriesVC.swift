//
//  ViewController.swift
//  ListRepositories
//
//  Created by Onur Yıldırım on 20.03.2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class ListRepositoriesVC: BaseVC {
    
    let disposeBag = DisposeBag()
    let viewModel: ListRepositoriesViewModel
    
    private lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchTextField.addDoneButtonOnKeyboard()
        
        searchBar.searchTextField
            .rx
            .text
            .orEmpty
            .throttle(.milliseconds(1000), scheduler: MainScheduler.instance)
            .bind(to: viewModel.searchText)
            .disposed(by: disposeBag)
        
        return searchBar
    }()
    
    private var tableView:UITableView! {
        didSet {
            if let tableView = self.tableView {
                tableView.estimatedRowHeight = 100
                tableView.rowHeight = UITableView.automaticDimension
                tableView.separatorStyle = .none
                
                tableView.tableFooterView = createActivityIndicator()
                tableView.tableFooterView!.isHidden = true
                viewModel.isBottomLoading
                    .subscribe(onNext: { tableView.tableFooterView?.isHidden = !$0 })
                    .disposed(by: disposeBag)
                
                tableView.register(RepositoryCell.self,
                                   forCellReuseIdentifier: String(describing: RepositoryCell.self))
                
                view.addSubview(tableView)
            }
        }
    }
    
    init(listRepositoriesViewModel: ListRepositoriesViewModel) {
        self.viewModel = listRepositoriesViewModel
        super.init(nibName: .none, bundle: .none)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initUI()
        setupUI()
        bindUI()
    }
    
    private func initUI() {
        view.addSubview(searchBar)
        tableView = UITableView()
    }
    
    private func setupUI() {
        searchBar.snp.makeConstraints { [unowned self] (m) in
            m.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            m.left.right.equalTo(view)
        }
        tableView.snp.makeConstraints { [unowned self] (m) in
            m.top.equalTo(searchBar.snp.bottom)
            m.left.right.bottom.equalTo(view.safeAreaLayoutGuide)
        }
    }
    
    private func bindUI() {
        viewModel
            .repositories
            .asObservable()
            .bind(to:tableView.rx.items(cellIdentifier: String(describing: RepositoryCell.self))){ [unowned self] (index, property, cell) in
                if let tempCell = cell as? RepositoryCell {
                    let repository = self.viewModel.repositories.value[index]
                    tempCell.avatarImageSelectedClosure = { viewModel.avatarSelected(index: $0) }
                    tempCell.configureUI(repository: repository, index: index)
                    cell.layoutIfNeeded()
                }
            }.disposed(by: disposeBag)
        
        tableView
            .rx
            .itemSelected
          .subscribe(onNext: { [weak self] indexPath in
            self?.viewModel.cellSelected(index: indexPath.row)
          }).disposed(by: disposeBag)
        
        tableView
            .rx
            .contentOffset
            .throttle(.milliseconds(100), scheduler: MainScheduler.instance)
            .subscribe(onNext: {[weak self] (contentOffset) in
                
                guard let self = self else {return}
                let height = self.tableView.frame.size.height
                let distanceFromBottom = self.tableView.contentSize.height - contentOffset.y
                if distanceFromBottom < height {
                    self.viewModel.endOfScrollTriggered()
                }
            }).disposed(by: disposeBag)
    }
}
