//
//  StringExtension.swift
//  Pockemon Search
//
//  Created by Onur Yıldırım on 17.01.2021.
//

import Foundation

extension String {
    
    func int() -> Int? {
        return Int(self)
    }
}
